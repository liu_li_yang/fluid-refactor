/*
 * @Project Name: fluid-refactor
 * @File Name: JmsMessageBrokerFactoryTest.java
 * @Package Name: com.paic.arch.jmsbroker
 * @Date: 2018年3月2日下午2:40:21
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker;

import org.junit.Test;

/**
 * Broker设计采用了工厂方法模式
 * @author liuliyang
 * @date 2018年3月2日下午2:40:21
 * @see
 */
public class JmsMessageBrokerContextTest {

	@Test
	public void blah() {
		String brokerUrl = "tcp://localhost:9000";
		String inputMessage = "message_test";
		String inputQueue = "queue_test";
		String accountingQueue = "queue_test";
		JmsMessageBrokerContext context = new JmsMessageBrokerContext();
		context.use(BrokerEnum.ACTIVE_MQ).bindToBrokerAtUrl(brokerUrl).and().sendTheMessage(inputMessage).to(inputQueue)
				.andThen().waitForAMessageOn(accountingQueue);
	}
}
