/*
 * @Project Name: fluid-refactor
 * @File Name: BrokerFactory.java
 * @Package Name: com.paic.arch.jmsbroker.factory
 * @Date: 2018年3月3日上午11:12:05
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker.factory;

import com.paic.arch.jmsbroker.product.JmsMessageBroker;

/**
 * Abstract Broker Factory
 * @author liuliyang-186
 * @date 2018年3月3日上午11:12:05
 * @see
 */
public interface JmsMessageBrokerFactory {

	public JmsMessageBroker builder();
}
