/*
 * @Project Name: fluid-refactor
 * @File Name: ActiveMqJmsMessageBrokerFactory.java
 * @Package Name: com.paic.arch.jmsbroker.factory
 * @Date: 2018年3月3日上午11:14:27
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker.factory;

import com.paic.arch.jmsbroker.product.ActiveMqJmsMessageBroker;
import com.paic.arch.jmsbroker.product.JmsMessageBroker;

/**
 * ActiveMq Concrete Broker Factory
 * @author liuliyang-186
 * @date 2018年3月3日上午11:14:27
 * @see
 */
public class ActiveMqJmsMessageBrokerFactory implements JmsMessageBrokerFactory {

	@Override
	public JmsMessageBroker builder() {
		return new ActiveMqJmsMessageBroker();
	}
}
