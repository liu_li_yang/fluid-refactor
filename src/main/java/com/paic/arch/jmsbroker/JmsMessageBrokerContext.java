/*
 * @Project Name: fluid-refactor
 * @File Name: JmsMessageBrokerFactory.java
 * @Package Name: com.paic.arch.jmsbroker
 * @Date: 2018年3月2日下午2:15:53
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.paic.arch.jmsbroker.factory.ActiveMqJmsMessageBrokerFactory;
import com.paic.arch.jmsbroker.factory.JmsMessageBrokerFactory;
import com.paic.arch.jmsbroker.factory.KafkaJmsMessageBrokerFactory;
import com.paic.arch.jmsbroker.product.JmsMessageBroker;

/**
 * Broker上下文，面向调用者
 * @author liuliyang
 * @date 2018年3月2日下午2:15:53
 */
public class JmsMessageBrokerContext {

	/**
	 * broker factory 容器</br>
	 * key: 具体产品，枚举名称 </br>
	 * value: 具体工厂</br>
	 */
	private Map<String, JmsMessageBrokerFactory> brokerFactoryMap = new ConcurrentHashMap<>();

	/**
	 * 构造函数初始化数据，如果需要，可以使用依赖注入的方式初始化
	 */
	public JmsMessageBrokerContext() {
		brokerFactoryMap.put(BrokerEnum.ACTIVE_MQ.getBrokerName(), new ActiveMqJmsMessageBrokerFactory());
		brokerFactoryMap.put(BrokerEnum.KAFKA.getBrokerName(), new KafkaJmsMessageBrokerFactory());
	}

	/**
	 * 通过定义枚举获取具体Broker
	 * @date 2018年3月3日上午11:42:00
	 * @author liuliyang
	 * @param brokerEnum Broker枚举
	 * @return 具体Broker
	 */
	public JmsMessageBroker use(BrokerEnum brokerEnum) {
		return brokerFactoryMap.get(brokerEnum.getBrokerName()).builder();
	}

	public void setBrokerFactoryMap(Map<String, JmsMessageBrokerFactory> brokerFactoryMap) {
		this.brokerFactoryMap = brokerFactoryMap;
	}
}
