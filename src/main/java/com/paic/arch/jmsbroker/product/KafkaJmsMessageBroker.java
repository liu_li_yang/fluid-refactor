/*
 * @Project Name: fluid-refactor
 * @File Name: ActiveMqJmsMessageBroker.java
 * @Package Name: com.paic.arch.jmsbroker.product
 * @Date: 2018年3月2日下午2:23:39
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker.product;

/**
 * Kafka Concrete Broker
 * @author liuliyang-186
 * @date 2018年3月2日下午2:23:39
 * @see
 */
public class KafkaJmsMessageBroker implements JmsMessageBroker {

	@Override
	public JmsMessageBroker bindToBrokerAtUrl(String brokerUrl) {
		return null;
	}

	@Override
	public JmsMessageBroker sendTheMessage(String inputMessage) {
		return null;
	}

	@Override
	public long waitForAMessageOn(String accountingQueue) {
		return 0;
	}

	@Override
	public JmsMessageBroker to(String inputQueue) {
		return null;
	}
}
