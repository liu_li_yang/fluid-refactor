/*
 * @Project Name: fluid-refactor
 * @File Name: JmsMessageBroker.java
 * @Package Name: com.paic.arch.jmsbroker.product
 * @Date: 2018年3月2日下午2:19:33
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker.product;

/**
 * Abstract Broker
 * @author liuliyang-186
 * @date 2018年3月2日下午2:19:33
 * @see
 */
public interface JmsMessageBroker {

	public JmsMessageBroker bindToBrokerAtUrl(String brokerUrl);

	default JmsMessageBroker and() {
		return this;
	}

	default JmsMessageBroker andThen() {
		return this;
	}

	public JmsMessageBroker sendTheMessage(String inputMessage);

	public JmsMessageBroker to(String inputQueue);

	public long waitForAMessageOn(String accountingQueue);
}
