/*
 * @Project Name: fluid-refactor
 * @File Name: ActiveMqJmsMessageBroker.java
 * @Package Name: com.paic.arch.jmsbroker.product
 * @Date: 2018年3月2日下午2:23:39
 * @Creator: liuliyang
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker.product;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;
import org.springframework.util.StringUtils;

/**
 * activeMq Concrete Broker
 * @author liuliyang
 * @date 2018年3月2日下午2:23:39
 * @see
 */
public class ActiveMqJmsMessageBroker implements JmsMessageBroker {

	private static final Logger LOG = getLogger(ActiveMqJmsMessageBroker.class);
	public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
	private String brokerUrl;
	private String inputMessage;
	private BrokerService brokerService;

	/**
	 * brokerUrl为空，使用默认的地址
	 */
	@Override
	public JmsMessageBroker bindToBrokerAtUrl(String brokerUrl) {
		LOG.debug("the bind brokerUrl is {}", brokerUrl);
		this.brokerUrl = StringUtils.isEmpty(brokerUrl)
				? DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000) : brokerUrl;
		return this;
	}

	@Override
	public JmsMessageBroker sendTheMessage(String inputMessage) {
		LOG.debug("the send inputMessage is {}", inputMessage);
		this.inputMessage = inputMessage;
		return this;
	}

	@Override
	public JmsMessageBroker to(String inputQueue) {
		try {
			this.createEmbeddedBroker();
			this.startEmbeddedBroker();
			executeCallbackAgainstRemoteBroker(brokerUrl, inputQueue, (aSession, aDestination) -> {
				MessageProducer producer = aSession.createProducer(aDestination);
				producer.send(aSession.createTextMessage(inputMessage));
				producer.close();
				return "";
			});
		} catch (Exception e) {
			LOG.error("Cannot send message {}", e.getMessage(), e);
		}
		return this;
	}

	@Override
	public long waitForAMessageOn(String accountingQueue) {
		try {
			return getEnqueuedMessageCountAt(accountingQueue);
		} catch (Exception e) {
			LOG.error("Cannot accounting queue {}", e.getMessage(), e);
		}
		return 0L;
	}

	private void createEmbeddedBroker() throws Exception {
		brokerService = new BrokerService();
		brokerService.setPersistent(false);
		brokerService.addConnector(brokerUrl);
	}

	private void startEmbeddedBroker() throws Exception {
		brokerService.start();
	}

	public void stopTheRunningBroker() throws Exception {
		if (brokerService == null) {
			throw new IllegalStateException("Cannot stop the broker from this API: "
					+ "perhaps it was started independently from this utility");
		}
		brokerService.stop();
		brokerService.waitUntilStopped();
	}

	private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName,
			JmsCallback aCallback) {
		Connection connection = null;
		String returnValue = "";
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
			connection = connectionFactory.createConnection();
			connection.start();
			returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
		} catch (JMSException jmse) {
			LOG.error("failed to create connection to {}", aBrokerUrl);
			throw new IllegalStateException(jmse);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException jmse) {
					LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
					throw new IllegalStateException(jmse);
				}
			}
		}
		return returnValue;
	}

	private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName,
			JmsCallback aCallback) {
		Session session = null;
		try {
			session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue(aDestinationName);
			return aCallback.performJmsFunction(session, queue);
		} catch (JMSException jmse) {
			LOG.error("Failed to create session on connection {}", aConnection);
			throw new IllegalStateException(jmse);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (JMSException jmse) {
					LOG.warn("Failed to close session {}", session);
					throw new IllegalStateException(jmse);
				}
			}
		}
	}

	private long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
		return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
	}

	private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
		Broker regionBroker = brokerService.getRegionBroker();
		for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
			if (destination.getName().equals(aDestinationName)) {
				return destination.getDestinationStatistics();
			}
		}
		throw new IllegalStateException(
				String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
	}
}
