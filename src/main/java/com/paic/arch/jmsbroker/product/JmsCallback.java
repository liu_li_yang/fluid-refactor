/*
 * @Project Name: fluid-refactor
 * @File Name: JmsCallback.java
 * @Package Name: com.paic.arch.jmsbroker.product
 * @Date: 2018年3月3日上午10:49:07
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker.product;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * 定义回调函数式接口
 * @author liuliyang
 * @date 2018年3月3日上午10:49:07
 * @see
 */
@FunctionalInterface
public interface JmsCallback {

	String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
}
