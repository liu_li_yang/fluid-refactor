/*
 * @Project Name: fluid-refactor
 * @File Name: BrokerEnum.java
 * @Package Name: com.paic.arch.jmsbroker
 * @Date: 2018年3月2日下午2:28:14
 * @Creator: liuliyang-186
 * @line------------------------------
 * @修改人: 
 * @修改时间: 
 * @修改内容: 
 */

package com.paic.arch.jmsbroker;

/**
 * @author liuliyang-186
 * @date 2018年3月2日下午2:28:14
 * @see
 */
public enum BrokerEnum {
	ACTIVE_MQ("activeMq"), KAFKA("kafka");

	private String brokerName;

	BrokerEnum(String brokerName) {
		this.brokerName = brokerName;
	}

	public String getBrokerName() {
		return brokerName;
	}
}
